//! Count strokes of both traditional and simplified text.

#![warn(missing_docs)]

use anyhow::Result;
use clap::{crate_version, Parser};
use clipboard::{ClipboardContext, ClipboardProvider};
use lazy_static::lazy_static;
use opencc_rust::{DefaultConfig, OpenCC};
use std::collections::HashMap;
use std::io;
use std::path::Path;

const STROKE: &str = include_str!("../resources/CNS_stroke.txt");
const UNICODE_FILE1: &str = include_str!("../resources/CNS2UNICODE_Unicode 15.txt");
const UNICODE_FILE2: &str = include_str!("../resources/CNS2UNICODE_Unicode 2.txt");
const UNICODE_FILE3: &str = include_str!("../resources/CNS2UNICODE_Unicode BMP.txt");
const UNICODE_FILES: &[&str] = &[UNICODE_FILE1, UNICODE_FILE2, UNICODE_FILE3];

lazy_static! {
    static ref STROKE_MAP: HashMap<&'static str, u32> = {
        let mut m = HashMap::new();
        for line in STROKE.trim().lines() {
            let (k, v) = line.split_at(line.find('\t').unwrap());
            m.insert(k, v.trim().parse().unwrap());
        }
        m
    };
    static ref UNICODE_MAP: HashMap<&'static str, &'static str> = {
        let mut m = HashMap::new();
        for file in UNICODE_FILES {
            for line in file.trim().lines() {
                let (v, k) = line.split_at(line.find('\t').unwrap());
                m.insert(k.trim(), v);
            }
        }
        m
    };
}

/// CLI options
#[derive(Parser, Debug)]
#[clap(version = crate_version!())]
#[clap(about = "Count strokes of both traditional and simplified text.")]
struct Opts {
    /// Path to file containing text
    #[clap(short, long, conflicts_with = "clipboard")]
    file_name: Option<String>,

    /// Grab text from clipboard
    #[clap(short, long)]
    clipboard: bool,
}
/// Convert to Simplified characters
fn to_simp(string: &str) -> String {
    let cc = OpenCC::new(DefaultConfig::TW2SP).unwrap();
    cc.convert(string)
}

/// Convert Chinese character to Unicode code point
fn to_unicode(c: &char) -> String {
    format!("{:x}", *c as u32).to_uppercase()
}

/// Count the strokes of every character in text
fn count_strokes(string: &str) -> (u32, u32) {
    let mut total_strokes = 0;
    let mut total_chars = 0;
    for char in string.trim().chars() {
        let unicode_point = to_unicode(&char);
        let cns_point = match get_cns_code(&unicode_point) {
            Some(s) => *s,
            None => continue,
        };
        let stroke_count = match STROKE_MAP.get(cns_point) {
            Some(i) => *i,
            None => continue,
        };
        total_strokes += stroke_count;
        total_chars += 1;
    }
    (total_strokes, total_chars)
}

/// Grab text from clipboard
fn get_clipboard() -> Result<String> {
    let mut clip: ClipboardContext =
        ClipboardProvider::new().expect("Can't create clipboard provider");
    let string = clip.get_contents().expect("Can't set clipboard");
    Ok(string)
}

/// Read contents from text file
fn read_file<P>(path: P) -> Result<String>
where
    P: AsRef<Path>,
{
    let text = std::fs::read_to_string(path)?;
    Ok(text)
}

/// Get CNS code for character
fn get_cns_code(unicode: &str) -> Option<&&str> {
    UNICODE_MAP.get(unicode)
}

fn main() -> Result<()> {
    let opts: Opts = Opts::parse();
    let mut trad = String::new();
    if opts.clipboard {
        trad = get_clipboard()?;
    } else if let Some(path) = opts.file_name {
        trad = read_file(path)?;
    } else {
        io::stdin().read_line(&mut trad)?;
    }
    let simp = to_simp(&trad);

    let (trad_strokes, trad_chars) = count_strokes(&trad);
    let (simp_strokes, simp_chars) = count_strokes(&simp);
    let trad_spc = trad_strokes as f32 / trad_chars as f32;
    let simp_spc = simp_strokes as f32 / simp_chars as f32;

    println!("\nTotal chars: {trad_chars}\n");

    println!(
        "Trad:\nTotal strokes: {trad_strokes}\nAverage strokes/char: {trad_spc:.1}\n"
    );
    println!(
        "Simp:\nTotal strokes: {simp_strokes}\nAverage strokes/char: {simp_spc:.1}\n"
    );

    println!(
        "Only {:.1} more strokes per character with traditional characters.",
        trad_spc - simp_spc
    );

    Ok(())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn unicode() {
        let char1 = '我';
        let char2 = '你';
        let char1_code = "6211";

        assert_eq!(char1_code, to_unicode(&char1));
        assert_ne!(char1_code, to_unicode(&char2));
    }
    #[test]
    fn cns() {
        let char1_uni = "6211";
        let char1_cns = "1-4A3C";

        assert_eq!(char1_cns, *get_cns_code(char1_uni).unwrap());
    }
}
